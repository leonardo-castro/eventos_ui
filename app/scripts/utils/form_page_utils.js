'use strict';

angular.module('eventosApp')
  .service('FormPageUtils', function ($http, $filter, ApplicationProperties, DataTableUtils, toastr) {
    var self = this;

    // a = action
    // action => "edit" | "new"
    self.isFormActionEqualsEdit = function (a) {
      return a === "edit";
    };

    // a = action
    // action => "edit" | "new"
    self.isFormActionEqualsNew = function (a) {
      return a === "new";
    };

    self.modelLoadErrorMessage = function(view) {
      toastr.error('Não foi possível carregar as informações de "' + view.name.singular + '(' + view.params.id + ')".', "Erro");
    };

    return self;
});