'use strict';

angular.module('eventosApp')
  .service('IndexPageUtils', function ($http, $filter, ApplicationProperties, DataTableUtils, toastr) {
    var self = this;

    // Se caso o usuário tenha sido redirecionado para a tela de listagem após ter efetuado
    // a inserção ou edição de um registro, este método se encarrega de exibir a mensagem de
    // operação realizada com sucesso
    //
    // PARAMS:
    //
    // self.view = {
    //   name: "Endereço",
    //   params: {
    //     s: $stateParams.s,
    //     u: $stateParams.u
    //   }
    // };
    self.showPreviouslySaveOrUpdateOperationSuccessMessage = function(view) {
      if( view.params.s ) {
        toastr.success($filter("capitalize")(view.name.singular) + ' adicionado com sucesso.');
      } else if ( view.params.u ) {
        toastr.success($filter("capitalize")(view.name.singular) + ' alterado com sucesso.');
      }
    };

    // Ao clicar no botão de excluir em uma linha do grid de listagem, uma requisição de delete
    // para o ID do objeto referente à aquela linha será engatilhada.
    // Se bem sucedida, para que não seja necessário fazer um novo "get" para preencher os dados do grid,
    // é removida essa linha no grid, procurando pelo index deste objeto na array pelo seu ID.
    self.removeFromGrid = function (arrayOfModelObjects, id, view) {
      var modelIndexToBeDeleted = $.map(arrayOfModelObjects, function(modelObject, index) {
        if(modelObject.id === parseInt(id)) {
          return index;
        }
      });

      if(modelIndexToBeDeleted.length) {
        arrayOfModelObjects.splice(modelIndexToBeDeleted[0], 1);
        toastr.success($filter("capitalize")(view.name.singular) + ' "' + id + '" excluído com sucesso.');
      } else {
        toastr.warning($filter("capitalize")(view.name.singular) + ' foi excluído da base de dados mas não da listagem.', 'Atenção');
      }
    };

    self.errorOnDestroy = function(id, view) {
      toastr.error('Houve uma falha ao tentar excluir o ' + $filter('lowercase')(view.name.singular) + '"' + id + '".', 'Erro');
    };

    return self;
});