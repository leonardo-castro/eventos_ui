'use strict';

angular.module('eventosApp')
  .service('StringUtils', function () {
    var self = this;

    self.inserirTextoEntreAspas = function(texto) {
      return '"' + texto + '"';
    };

    return self;
});