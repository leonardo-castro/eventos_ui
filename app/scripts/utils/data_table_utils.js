'use strict';

angular.module('eventosApp')
  .service('DataTableUtils', function ($filter, DTOptionsBuilder, DTColumnDefBuilder) {
    var self = this;

    // 'angular-datatables' precisa de um objeto 'dtOptions' que define todas as configurações da tabela a ser personalizada
    // pelo componente 'datatable' do jquery.
    // Este método foi criado para não precisar repetir essa configuração a cada nova instância de tabela
    self.loadDtOptions = function() {
      var dtOptions =
        DTOptionsBuilder.newOptions()
          .withDOM("<'row'<'col-md-6 col-sm-6'><'col-md-6 col-sm-6'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-5'i><'col-md-7 col-sm-7'p>>")
          .withOption("oClasses", {
            "sWrapper": "dataTables_wrapper",
            "sFilterInput": "form-control input-small input-inline",
            "sLengthSelect": "form-control input-xsmall input-inline"
          })
          .withDisplayLength(100)
          .withBootstrap()
          .withLanguage({
            "sEmptyTable": $filter("translate")("datatable.empty_table"),
            "sInfo": $filter("translate")("datatable.info"),
            "sInfoEmpty": $filter("translate")("datatable.info_empty"),
            "sInfoFiltered": $filter("translate")("datatable.info_filtered"),
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": $filter("translate")("datatable.length_menu"),
            "sLoadingRecords": $filter("translate")("datatable.loading"),
            "sProcessing": $filter("translate")("datatable.processing"),
            "sSearch": $filter("translate")("datatable.search"),
            "sZeroRecords": $filter("translate")("datatable.zero_records"),
            "oPaginate": {
                "sFirst": $filter("translate")("datatable.paginate.first"),
                "sLast": $filter("translate")("datatable.paginate.last"),
                "sPrevious": '<i class="fa fa-angle-left"></i>',
                "sNext": '<i class="fa fa-angle-right"></i>'
            },
            "oAria": {
                "sSortAscending": $filter("translate")("datatable.aria.sort_ascending"),
                "sSortDescending": $filter("translate")("datatable.aria.sort_descending")
            }
          });

      return dtOptions;
    };

    // angular-datatable define suas colunas a partir de um objeto chamado "DTColumnDefBuilder"
    // para ter um grid com 6 colunas, é preciso que o objeto 'dtColumnDefs' tenha 6 objetos de
    // "DTColumnDefBuilder.newColumnDef(columnIndex)"
    //
    // ex:
    // dtColumnDefs = [
    // DTColumnDefBuilder.newColumnDef(0),
    // DTColumnDefBuilder.newColumnDef(1),
    // DTColumnDefBuilder.newColumnDef(2),
    // DTColumnDefBuilder.newColumnDef(3),
    // DTColumnDefBuilder.newColumnDef(4),
    // DTColumnDefBuilder.newColumnDef(5)
    // ]
    self.loadColumnDefs = function (columnsQuantity) {
        var dtColumnDefs = [];

        var columnIndex;
        for( columnIndex = 0; columnIndex < columnsQuantity; columnIndex++) {
          dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(columnIndex));
        }

        return dtColumnDefs;
    };

    return self;
});