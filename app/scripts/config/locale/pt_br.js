var translate_table_pt_br = {
"datatable":{
  "aria": {
    "sort_ascending": "ative para ordenação crescente",
    "sort_descending": "ative para ordenação decrescente"
  },
    "empty_table": "Tabela sem dados",
    "info":"Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "info_empty":"Mostrando de 0 até 0 de 0 registros",
    "info_filtered":"(filtrado de _MAX_ registros no total)",
    "info_post_fix":"",
    "length_menu":"_MENU_ registros",
    "loading": "Carregando...",
    "paginate":{
    "first":"Primeiro",
    "last":"Último",
    "next":"Seguinte",
    "previous":"Anterior"
  },
    "processing":"Processando...",
    "search":"Buscar",
    "url":"",
    "zero_records":"Não foram encontrados resultados"
  }
}