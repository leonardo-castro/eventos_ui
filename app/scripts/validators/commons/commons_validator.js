'use strict';

// Parâmetro base dos metodos: "exceptions"
// "exceptions": array simples de strings que contempla a cada posição, uma mensagem de erro
angular.module('eventosApp')
  .service('CommonsValidator', function (StringUtils) {
    var self = this;

    // Verifica se o atributo "exceptions" do validator em questão contém erros ou não
    // return: boolean
    self.hasNoErrors = function(exceptions) {
      // "exceptions.length" é equivalente a verificação "exceptions.length > 0"
      if( exceptions.length ) {
        return false;
      }

      return true;
    }

    /*
    VALIDATION METHODS
    */

    // Contempla as seguintes verificações:
    //    self.stringIsEmpty
    //    self.stringLengthLongerThan255
    // params:
    // exceptions => objeto de exceptions que vem da view
    // attributeTitle => É o "label" do atributo, ou seja, o título que será exibido na mensagem de erro
    // attributeName => É a chave do json que referencia o atributo
    // string => texto a ser validado
    // return: void
    self.stringBaseValidation = function(exceptions, attributeTitle, attributeName, string) {
      var message = "";

      if(!string) {
        message = self.getStringIsEmptyMessage();
        self.addNewException(exceptions, message, attributeTitle, attributeName);
      } else {
        if(string.length > 255 ) {
          message = self.getStringLengthLongerThan255Message(string.length);
          self.addNewException(exceptions, message, attributeTitle, attributeName);
        }
      }
    }

    self.hasNumbersValidation = function (string) {
      var hasNumbers = /\d/;
      return hasNumbers.test(string);
    }


    /*
    GET MESSAGE METHODS
    */

    // Retorna a mensagem de string vazia
    self.getStringIsEmptyMessage = function() {
      return "não pode ser vazio.";
    }

    // "attributeLength": string que representa a quantidade de caracteres do atributo em questão, exemplo: 256
    // return: '"Rua" não pode ultrapassar o limite de 255 caracteres. ( total caracteres: 256 )'
    self.getStringLengthLongerThan255Message = function(attributeLength) {
      return "não pode ultrapassar o limite de 255 caracteres ( total caracteres: " + attributeLength +  ").";
    }

    self.getStringDoesntHasNumbersMessage = function (attributeValue) {
      if(attributeValue === undefined) {
        attributeValue = ""; // Para não exibir "( valor anterior: "undefined" )."
      }

      return 'precisa conter números ( valor anterior: "' + attributeValue + '" ).';
    }

    /*
    OTHER METHODS
    */
    // exceptions => array de objetos "exception" do validador em questão, ex: EnderecosValidator
    // message => "não pode ser vazio"
    // title => "Rua"
    // name => "rua" (chave do json que referencia o atributo)
    self.addNewException = function(exceptions, message, title, name) {
      var exception = { message: message, title: title, name: name };
      exceptions.push(exception);
    }

    // Verifica se há algum objeto "exception" que contenha dentro dele
    // o atributo "name" preenchido com o nome do atributo em questão
    // indicando que o mesmo passou pelo método "validate" de seu validator
    // e após a verificação, constou-se que o mesmo tem erros
    self.attributeHasErrors = function(exceptions, attributeName) {
      var hasFoundTheExceptionOfThatAttribute = exceptions.filter(function(exception) {
        return exception.name === attributeName;
      });

      if(hasFoundTheExceptionOfThatAttribute.length) {
        return true;
      }

      return false;
    }

    return self;
});