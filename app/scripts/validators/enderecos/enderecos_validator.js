  'use strict';

  // {
  //   "id": 1,
  //   "rua": "Rua Dona Neli dos Santos Lopes",
  //   "bairro": "Vila Industrial",
  //   "numero": "15",
  //   "cep": "12220-350",
  //   "dataCriacao": "28-10-2015 23:57:06",
  //   "dataAlteracao": null
  // }

angular.module('eventosApp')
  .service('EnderecosValidator', function (CommonsValidator) {
    var self = this;

    // =====
    // = INIT
    // =====
    self.exceptions = [];

    self.validate = function(endereco) {
      self.exceptions = [];

      // Rua
      CommonsValidator.stringBaseValidation(self.exceptions, "Rua", "rua", endereco.rua);

      // Bairro
      CommonsValidator.stringBaseValidation(self.exceptions, "Bairro", "bairro", endereco.bairro);

      // Número
      CommonsValidator.stringBaseValidation(self.exceptions, "Número", "numero", endereco.numero);
      if(CommonsValidator.hasNumbersValidation(endereco.numero) == false) {
        var message, title = "";
        message = CommonsValidator.getStringDoesntHasNumbersMessage(endereco.numero);
        title = "Número";

        CommonsValidator.addNewException(self.exceptions, message, title, "numero");
      }

      // CEP
      CommonsValidator.stringBaseValidation(self.exceptions, "CEP", "cep", endereco.cep);
      if(CommonsValidator.hasNumbersValidation(endereco.cep) == false) {
        var message, title = "";
        message = CommonsValidator.getStringDoesntHasNumbersMessage(endereco.cep);
        title = "CEP";

        CommonsValidator.addNewException(self.exceptions, message, title, "cep");
      }
    }

    self.isValidForCreation = function(endereco) {
      self.validate(endereco);
      return CommonsValidator.hasNoErrors(self.exceptions);
    }

    self.isValidForUpdate = function(endereco) {
      self.validate(endereco);
      return CommonsValidator.hasNoErrors(self.exceptions);
    }

    self.changeModelHasErrorsFlags = function() {
      var hasErrors = {
        rua: CommonsValidator.attributeHasErrors(self.exceptions, "rua"),
        bairro: CommonsValidator.attributeHasErrors(self.exceptions, "bairro"),
        numero: CommonsValidator.attributeHasErrors(self.exceptions, "numero"),
        cep: CommonsValidator.attributeHasErrors(self.exceptions, "cep")
      }

      return hasErrors;
    }

    return self;
});