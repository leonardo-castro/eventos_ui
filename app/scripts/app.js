'use strict';

/**
 * @ngdoc overview
 * @name eventosApp
 * @description
 * # eventosApp
 *
 * Main module of the application.
 */
angular
  .module('eventosApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'datatables',
    'datatables.bootstrap',
    'datatables.columnfilter',
    'angular-loading-bar',
    'pascalprecht.translate',
    'toastr',
    'angular-capitalize-filter'
  ])
  // toastr
  .config(function(toastrConfig) {
    angular.extend(toastrConfig, {
      allowHtml: true,
      closeButton: true,
      closeHtml: '<button>&times;</button>',
      extendedTimeOut: 1000,
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      messageClass: 'toast-message',
      onHidden: null,
      onShown: null,
      onTap: null,
      progressBar: true,
      tapToDismiss: false,
      templates: {
        toast: 'directives/toast/toast.html',
        progressbar: 'directives/progressbar/progressbar.html'
      },
      timeOut: 5000,
      titleClass: 'toast-title',
      toastClass: 'toast'
    });
  })

  // i18n
  .config(function ($translateProvider) {
    // Translate variables can be found at "app/config/locale/"
    $translateProvider
      .translations('pt-br', translate_table_pt_br)
      .preferredLanguage('pt-br');
  })

  // Set custom header for all $http requests
  .config(function($httpProvider) {
    $httpProvider
      .defaults.headers.common = {"Content-Type":"application/json"};
  })

  // STATE PROVIDER - PARAMETERS
  // https://github.com/angular-ui/ui-router/wiki/URL-Routing
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('main', {
        url: "/",
        templateUrl: "views/main.html",
        controller: 'MainCtrl',
        controllerAs: 'main'
      })

      // ===========
      // = CIDADES
      // ===========
      .state('cidades_form', {
        url: "/cidades/form",
        templateUrl: "views/cidades/cidades_form.html",
        controller: 'CidadesFormController',
        controllerAs: 'cidadesCtrl',
        params: {
          a: "new" // a = action
        }
      })
      .state('cidades_index', {
        url: "/cidades",
        templateUrl: "views/cidades/cidades_index.html",
        controller: 'CidadesIndexController',
        controllerAs: 'cidadesCtrl'
      })

      // ===========
      // = ENDERECOS
      // ===========
      .state('enderecos_form', {
        url: "/enderecos/form",
        templateUrl: "views/enderecos/enderecos_form.html",
        controller: 'EnderecosFormController',
        controllerAs: 'enderecosCtrl',
        params: {
          a: "new", // a = action [new, edit]
          id: undefined
        }
      })
      .state('enderecos_index', {
        url: "/enderecos",
        templateUrl: "views/enderecos/enderecos_index.html",
        controller: 'EnderecosIndexController',
        controllerAs: 'enderecosCtrl',
        params: {
          s: false, // s = save
          u: false // u = update
        }
      })

      // ===========
      // = EVENTOS
      // ===========
      .state('eventos_form', {
        url: "/eventos/form",
        templateUrl: "views/eventos/eventos_form.html",
        controller: 'EventosFormController',
        controllerAs: 'eventosCtrl',
        params: {
          a: "new" // a = action
        }
      })
      .state('eventos_index', {
        url: "/eventos",
        templateUrl: "views/eventos/eventos_index.html",
        controller: 'EventosIndexController',
        controllerAs: 'eventosCtrl'
      })

      // ===========
      // = PUBLICO_ALVO
      // ===========
      .state('publico_alvo_form', {
        url: "/publico_alvo/form",
        templateUrl: "views/publico_alvo/publico_alvo_form.html",
        controller: 'PublicoAlvoFormController',
        controllerAs: 'publico_alvoCtrl',
        params: {
          a: "new" // a = action
        }
      })
      .state('publico_alvo_index', {
        url: "/publico_alvo",
        templateUrl: "views/publico_alvo/publico_alvo_index.html",
        controller: 'PublicoAlvoIndexController',
        controllerAs: 'publicoAlvoCtrl'
      });

      $urlRouterProvider.otherwise("/");
  });
