'use strict';

angular.module('eventosApp')
  .controller('EnderecosIndexController', function ($stateParams, $http, $state, ApplicationProperties, DataTableUtils, IndexPageUtils, toastr, $filter) {
    self = this;

    self.enderecos = {};
    self.model = {
      entityName: {
        plural: "enderecos",
        singular: "endereco"
      }
    };
    self.view = {
      errors: {
        onInit: false
      },
      name: {
        plural: "endereços",
        singular: "endereço"
      },
      params: {
        s: $stateParams.s,
        u: $stateParams.u
      }
    };


    self.init = function () {
      IndexPageUtils.showPreviouslySaveOrUpdateOperationSuccessMessage(self.view);

      var url = ApplicationProperties.apiUrl + "/" + self.model.entityName.plural;

      self.dtOptions = DataTableUtils.loadDtOptions();
      self.dtColumnDefs = DataTableUtils.loadColumnDefs(6);

      $http.get(url).then(function successCallback(response) {
          self.enderecos = response.data;
        }, function errorCallback(response) {
          self.errors.onInit = true;
        });
    }


    self.model.destroy = function (id) {
      var url = ApplicationProperties.apiUrl + "/" + self.model.entityName.plural + "/" + id;

      $http.delete(url).then(function successCallback(response) {
          IndexPageUtils.removeFromGrid(self.enderecos, id, self.view);
        }, function errorCallback(response) {
          IndexPageUtils.errorOnDestroy(id, self.view);
        });
    }

    self.init();
  });
