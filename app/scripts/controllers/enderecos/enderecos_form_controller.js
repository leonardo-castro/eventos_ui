'use strict';

  // {
  //   "id": 1,
  //   "rua": "Rua Dona Neli dos Santos Lopes",
  //   "bairro": "Vila Industrial",
  //   "numero": "15",
  //   "cep": "12220-350",
  //   "dataCriacao": "28-10-2015 23:57:06",
  //   "dataAlteracao": null
  // }

// self.params = $stateParams;
angular.module('eventosApp')
  .controller('EnderecosFormController', function ($stateParams, $http, $state, ApplicationProperties, EnderecosValidator, FormPageUtils) {
    var self = this;

    // =====
    // = INIT
    // =====
    self.endereco = {};
    self.view = {
      action: {
        isEdit: FormPageUtils.isFormActionEqualsEdit($stateParams.a),
        isNew: FormPageUtils.isFormActionEqualsNew($stateParams.a)
      },
      errors: [],
      name: {
        plural: "endereços",
        singular: "endereço"
      },
      params: {
        a: $stateParams.a, // edit | new
        id: $stateParams.id
      }
    };
    self.model = {
      hasErrors: {
        rua: false,
        bairro: false,
        numero: false,
        cep: false
      }
    }

    self.init = function() {
      if(self.view.action.isEdit) {
        self.model.load();
      }
    }

    self.model.load = function() {
      var url = ApplicationProperties.apiUrl + "/enderecos/" + self.view.params.id;

      $http.get(url).then(
        function successCallback(response) {
          self.endereco = response.data;
        },
        function errorCallback(response) {
          angular.element('#btnSalvar').prop("disabled", true);
          FormPageUtils.modelLoadErrorMessage(self.view);
        });
    }

    self.model.save = function() {
      // Desativa o botão de salvar até o final da operação
      angular.element('#btnSalvar').prop("disabled", true);

      var isValid = self.view.action.isNew ?
        EnderecosValidator.isValidForCreation(self.endereco) : EnderecosValidator.isValidForUpdate(self.endereco);

      if ( isValid ) {
        self.view.action.isNew ? self.view.sendPost() : self.view.sendPut();
      } else {
        // Ativa o botão novamente pois os atributos do formulário não são validos
        self.view.invalidateForm();
      }
    }

    // =====
    // = VIEW FUNCTIONS
    // =====
    self.view.sendPost = function() {
      var url = ApplicationProperties.apiUrl + "/enderecos";
      var json = angular.toJson(self.endereco);

      $http.post(url, json)
        .success(function(data) {
          // Redireciona para a tela listagem se o endereço for salvo com sucesso
          // "params.s = true" significa que a tela de index foi carregada a partir de uma operação de "save" do formuĺário
          var params = { s: true };
          $state.go('enderecos_index', params);
        })
        .error(function(data) {
          // Ativa o botão novamente pois o endereço não foi salvo e habilita o formulário para uma nova tentativa de salvar os dados
          self.view.invalidateForm();
        });
    }

    self.view.sendPut = function() {
      var url = ApplicationProperties.apiUrl + "/enderecos/" + self.view.params.id;
      var json = angular.toJson(self.endereco);

      $http.put(url, json)
        .success(function(data) {
          // Redireciona para a tela listagem se o endereço for atualizado com sucesso
          // "params.u = true" significa que a tela de index foi carregada a partir de uma operação de "update" do formuĺário
          var params = { u: true };
          $state.go('enderecos_index', params);
        })
        .error(function(data) {
          // Ativa o botão novamente pois o endereço não foi atualizado e habilita o formulário para uma nova tentativa de salvar os dados
          self.view.invalidateForm();
        });
    }

    self.view.invalidateForm = function() {
      angular.element('#btnSalvar').prop("disabled", false);
      self.view.showErrors();
      self.view.changeModelAttributesWithErrorsInputsColors();
    }

    self.view.showErrors = function () {
      self.view.errors = EnderecosValidator.exceptions;
    }

    self.view.changeModelAttributesWithErrorsInputsColors = function() {
      self.model.hasErrors = EnderecosValidator.changeModelHasErrorsFlags();
    }

    self.init();
  });
