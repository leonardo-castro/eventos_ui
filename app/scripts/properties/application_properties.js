  'use strict';

angular.module('eventosApp')
  .service('ApplicationProperties', function () {
    var self = this;

    self.apiUrl = "http://localhost:8080";

    return self;
  });